# libreBMC README #

This repository will contain the high level documentaion for libreBMC

**The overall goal of the project.** We want to boot an AC922 using an FPGA (not the AST2500 ASIC) on a DC-SCM card.  We want to prove that the FPGA can boot a high power, modern server.  

To do this, we need the following pieces:
1.	A DC-SCM card that has an FPGA on it to replace the AST2500 ASIC
    * Antmicro built some of these that use Xylinx A7s.  They power on, but no bring-up has been done to show they will boot the system.
    * Antmicro is building some that have ECP5’s on them, but they are still missing some parts
2.	An interposer that will plug into the DC-SCM card and the AC922. https://git.openpower.foundation/librebmc/ac922interposer.git
    * I have 6 of these built 
3.	A Root of Trust bypass jumper https://github.com/antmicro/dc-scm-rot-jumper-card
    * Plans for these are available and they are easy to build 
4.	A softcore (microwatt) running on the FPGA
    * **This works, but it needs to be better incorporated with Lite-x**
5.	**Full gateware in Lite-x for that FPGA such that the entire OpenBMC stack can run**
    * Some modules are done.  Some not.  https://git.openpower.foundation/librebmc/gateware.git
    * **Need build instructions for the full FPGA image**
6.  **Kernel Driver updates**
    * IPMI-BT done but needs merging.
    * LPC-CTRL.  Set up the address window.  Needs to be written from scratch.
7.	**Need the full OpenBMC code running**
    * We currently power on our prototype with scripts.  OpenBMC is not running.
    * https://git.openpower.foundation/librebmc/openbmc_for_AC922.git
8. **Bring-up of the AC922 with the DC-SCM card running OpenBMC on the FPGA**
    * Create build instructions for pulling the entire FPGA image together
    * Boot the AC922
    * Verify all functions
    * Prove there are no performance/timing issues
        * Need tests defined
    * Video the bringup/running system for display at conferences

So far we did a prototype where we used the xylinx A7 FPGA on a custom breadboard and ran microwatt and a very stripped down FPGA.  We proved it could boot the AC922, but again it was very low function.  We need to have it fully functional to really prove it.  We did not run OpenBMC on this prototype

Need to add here:
1. An overall architecural picture and description.  TBD
2. Links to pitches and presentations on libreBMC.  TBD
